module.exports = {
  env: {
    browser: true,
    es6: true,
  },
  parserOptions: {
    tsconfigRootDir: __dirname,
    project: ['./tsconfig.json'],
  },
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
  },
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaVersion: 2018,
  },
  extends: [
    '@rfermann/eslint-config',
    '@rfermann/eslint-config/requires-typechecking',
  ],
};
