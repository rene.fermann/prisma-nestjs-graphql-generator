export const pascalCase = (word: string): string =>
  `${word.charAt(0).toLocaleUpperCase()}${word.slice(1)}`;
