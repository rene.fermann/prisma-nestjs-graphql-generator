export const camelCase = (word: string): string =>
  `${word.charAt(0).toLocaleLowerCase()}${word.slice(1)}`;
