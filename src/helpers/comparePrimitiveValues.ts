export const comparePrimitiveValues = (a: string | number, b: string | number): number => {
  if (a > b) {
    return 1;
  }

  if (a < b) {
    // eslint-disable-next-line @typescript-eslint/no-magic-numbers
    return -1;
  }

  return 0;
};
