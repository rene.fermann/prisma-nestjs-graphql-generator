import type { DMMF } from "@prisma/generator-helper";
import { camelCase, pascalCase } from "../helpers";
import type { ParserConstructorInput, Resolver } from "../types";
import { OperationType } from "../types";
import { BaseParser } from "./BaseParser";

const getOperationKey = <T, TK extends keyof T>(obj: T, key: TK): T[TK] => obj[key];

export class ResolverParser extends BaseParser {
  private readonly _operationsMapping: { [key: string]: string } = {};

  private readonly _outputTypes: DMMF.OutputType[];

  constructor({ config, dmmf }: ParserConstructorInput) {
    super({ config, dmmf });

    this._outputTypes = this.dmmf.schema.outputObjectTypes.prisma.filter(({ name }) => {
      if (name === OperationType.Mutation || name === OperationType.Query) {
        return true;
      }

      return false;
    });

    this.dmmf.mappings.modelOperations.forEach((modelOperation) => {
      Object.keys(modelOperation).forEach((key) => {
        const operationKey = getOperationKey(modelOperation, key as keyof DMMF.ModelMapping);

        if (typeof operationKey === "string") {
          this._operationsMapping[operationKey] = key;
        }
      });
    });
  }

  parse(): Resolver[] {
    const resolvers: Resolver[] = [];

    this._outputTypes.forEach(({ fields, name }) => {
      fields
        .filter(({ deprecation, name: fieldName }) => {
          if (deprecation) {
            return false;
          }

          if (
            this.dmmf.mappings.otherOperations.read.includes(fieldName) ||
            this.dmmf.mappings.otherOperations.write.includes(fieldName)
          ) {
            return false;
          }

          return true;
        })
        .forEach(
          ({ isNullable, outputType: { isList, location, type }, ...resolver }) => {
            const fieldName = pascalCase(resolver.name);

            resolvers.push({
              fieldType: this.parseTSFieldType({
                enums: [],
                field: {
                  isList,
                  location,
                  type,
                },
                fieldOptions: {
                  isInputType: false,
                  isRequired: false,
                },
              }),
              graphQLType: this.parseGraphQLType([{ isList, location, type: type as string }]),
              inputType: this.getInputTypeName(fieldName),
              isNullable: typeof isNullable === "boolean" ? isNullable : false,
              method: fieldName,
              model: this.getModel(camelCase(fieldName)),
              name: this.getResolverName(fieldName),
              operation: this._operationsMapping[camelCase(fieldName)],
              operationType: name as OperationType,
            });
          }
        );
    });

    return resolvers;
  }
}
