import { compareArrayOfObjects } from "../helpers";
import type { Enum, EnumValue } from "../types";

import { BaseParser } from "./BaseParser";

export class EnumParser extends BaseParser {
  parse(): Enum[] {
    const enums: Enum[] = [];

    this.dmmf.datamodel.enums.forEach(({ documentation, name, values }): void => {
      enums.push({
        documentation,
        name: this.getEnumName(name),
        values: values.map(
          ({ name: value }): EnumValue => {
            return {
              name: value,
              value,
            };
          }
        ),
      });
    });

    this.dmmf.schema.enumTypes.prisma.forEach(({ name, values }): void => {
      if (enums.find((e) => this.getEnumName(e.name) === this.getEnumName(name)) === undefined) {
        enums.push({
          documentation: undefined,
          name: this.getEnumName(name),
          values: values.map(
            (value): EnumValue => {
              return {
                name: value,
                value,
              };
            }
          ),
        });
      }
    });

    return enums.sort((a, b) => compareArrayOfObjects({ a, b, field: "name" }));
  }
}
