import type { Enum, Field, InputType, ParserConstructorInput, ResolverInputType } from "../types";
import { NestJSTypes, OperationType, TypeEnum } from "../types";
import { BaseParser } from "./BaseParser";

interface InputParseReturnType {
  inputTypes: InputType[];
  resolverInputTypes: ResolverInputType[];
}

export class InputTypeParser extends BaseParser {
  private readonly _inputTypes: InputType[];

  private readonly _resolverInputTypes: ResolverInputType[];

  constructor({ config, dmmf }: ParserConstructorInput) {
    super({ config, dmmf });
    this._inputTypes = [];
    this._resolverInputTypes = [];
  }

  parse(enums: Enum[]): InputParseReturnType {
    this._parseInputTypes(enums);
    this._parseResolverInputTypes(enums);
    return {
      inputTypes: this._inputTypes,
      resolverInputTypes: this._resolverInputTypes,
    };
  }

  // eslint-disable-next-line max-lines-per-function
  private _parseInputTypes(enums: Enum[]): void {
    // eslint-disable-next-line max-lines-per-function
    this.dmmf.schema.inputObjectTypes.prisma.forEach((inputType) => {
      const fields: Field[] = [];
      const inputTypeImports: Set<string> = new Set();
      const jsonImports: Set<string> = new Set();
      const enumImports: Set<string> = new Set();
      const nestJSImports: Set<string> = new Set();

      nestJSImports.add(NestJSTypes.Field);
      nestJSImports.add(NestJSTypes.InputType);

      inputType.fields.forEach((field) => {
        const graphQLType = this.parseGraphQLType(field.inputTypes);
        const currentInputType = this.selectInputType(field.inputTypes);

        const parsedField = this.parseField({
          enums,
          field: {
            ...field,
            ...currentInputType,
            isInputType: true,
            isList: currentInputType.isList,
            type: currentInputType.type as string,
          },
          graphQLType,
        });

        fields.push(parsedField);

        field.inputTypes.forEach((inputTypeField) => {
          const type = inputTypeField.type as string;

          if (inputTypeField.location === "enumTypes") {
            enumImports.add(this.getEnumName(inputTypeField.type as string));
          }

          if (
            inputTypeField.location === "scalar" &&
            this.jsonImports.has(inputTypeField.type as string)
          ) {
            jsonImports.add(inputTypeField.type as string);
          }

          if (this.jsonImports.has(parsedField.tsType.split(" | ")[0])) {
            jsonImports.add(parsedField.tsType.split(" | ")[0]);
          }

          if (
            inputTypeField.location === "inputObjectTypes" &&
            inputTypeField.type !== inputType.name &&
            this.inputTypeList.has(type)
          ) {
            inputTypeImports.add(type);
          }

          if (this.nestJSImports.has(graphQLType)) {
            nestJSImports.add(graphQLType);
          }
        });
      });

      this._inputTypes.push({
        enumImports: Array.from(enumImports),
        fields,
        inputTypeImports: Array.from(inputTypeImports),
        jsonImports: Array.from(jsonImports),
        name: inputType.name,
        nestJSImports: Array.from(nestJSImports),
        type: TypeEnum.InputType,
      });
    });
  }

  // eslint-disable-next-line max-lines-per-function
  private _parseResolverInputTypes(enums: Enum[]): void {
    this.dmmf.schema.outputObjectTypes.prisma
      .filter(({ name }) => {
        if (name === OperationType.Mutation || name === OperationType.Query) {
          return true;
        }

        return false;
      })
      // eslint-disable-next-line max-lines-per-function
      .forEach((outputType) => {
        outputType.fields
          .filter(({ deprecation, name }) => {
            if (deprecation) {
              return false;
            }
            
            if (
              this.dmmf.mappings.otherOperations.read.includes(name) ||
              this.dmmf.mappings.otherOperations.write.includes(name)
            ) {
              return false;
            }

            return true;
          })
          // eslint-disable-next-line max-lines-per-function
          .forEach(({ args, name }) => {
            const fields: Field[] = [];
            const enumImports: Set<string> = new Set();
            const inputTypeImports: Set<string> = new Set();
            const jsonImports: Set<string> = new Set();
            const nestJSImports: Set<string> = new Set();

            nestJSImports.add(NestJSTypes.Field);
            nestJSImports.add(NestJSTypes.InputType);

            args.forEach(({ inputTypes, ...arg }) => {
              inputTypes.forEach((inputTypeField) => {
                if (this.inputTypeList.has(inputTypeField.type as string)) {
                  inputTypeImports.add(inputTypeField.type as string);
                }
              });

              const graphQLType = this.parseGraphQLType(inputTypes);
              const inputType = this.selectInputType(inputTypes);
              const { location, type } = inputType;

              const parsedField = this.parseField({
                enums,
                field: {
                  ...arg,
                  ...inputType,
                  isInputType: true,
                  type: type as string,
                },
                graphQLType,
              });

              if (location === "enumTypes") {
                enumImports.add(this.getEnumName(type as string));
              }

              if (location === "scalar" && this.jsonImports.has(type as string)) {
                jsonImports.add(type as string);
              }

              if (this.jsonImports.has(parsedField.tsType.split(" | ")[0])) {
                jsonImports.add(parsedField.tsType.split(" | ")[0]);
              }

              if (this.nestJSImports.has(graphQLType)) {
                nestJSImports.add(graphQLType);
              }

              fields.push(parsedField);
            });

            this._resolverInputTypes.push({
              enumImports: Array.from(enumImports),
              fields,
              inputTypeImports: Array.from(inputTypeImports),
              jsonImports: Array.from(jsonImports),
              model: this.getModel(name),
              name: this.getInputTypeName(name),
              nestJSImports: Array.from(nestJSImports),
              type: TypeEnum.ResolverInputType,
            });
          });
      });
  }
}
