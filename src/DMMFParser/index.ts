// eslint-disable-next-line import/no-nodejs-modules, import/no-unresolved
import { writeFile } from "fs/promises";

import type { DMMF } from "@prisma/generator-helper";

import type {
  Enum,
  GeneratorConfig,
  InputType,
  Model,
  OutputType,
  Resolver,
  ResolverInputType,
} from "../types";

import { EnumParser } from "./EnumParser";
import { InputTypeParser } from "./InputTypeParser";
import { ModelParser } from "./ModelParser";
import { OutputTypeParser } from "./OutputTypeParser";
import { ResolverParser } from "./ResolverParser";

export class DMMFParser {
  enums!: Enum[];

  inputTypes: InputType[];

  models: Model[];

  outputTypes: OutputType[];

  resolverInputTypes: ResolverInputType[];

  resolvers: Resolver[];

  private readonly _config: GeneratorConfig;

  private _enumParser!: EnumParser;

  private _inputTypeParser!: InputTypeParser;

  private _modelParser!: ModelParser;

  private _outputTypeParser!: OutputTypeParser;

  private _resolverParser!: ResolverParser;

  constructor(config: GeneratorConfig) {
    this._config = config;
    this.enums = [];
    this.inputTypes = [];
    this.models = [];
    this.outputTypes = [];
    this.resolverInputTypes = [];
    this.resolvers = [];
  }

  async init(prismaClientImportPath: string): Promise<void> {
    // dmmf needs to be imported from prisma client. content differs from the dmmf
    // passed in the generator options
    // eslint-disable-next-line import/dynamic-import-chunkname
    const dmmf = (await import(prismaClientImportPath)).dmmf as DMMF.Document;

    if (this._config.exportDMMF) {
      // eslint-disable-next-line @typescript-eslint/no-magic-numbers
      await writeFile(`${this._config.basePath}/dmmf.json`, JSON.stringify(dmmf, null, 2));
    }

    this._enumParser = new EnumParser({ config: this._config, dmmf });
    this._inputTypeParser = new InputTypeParser({ config: this._config, dmmf });
    this._modelParser = new ModelParser({ config: this._config, dmmf });
    this._outputTypeParser = new OutputTypeParser({ config: this._config, dmmf });
    this._resolverParser = new ResolverParser({ config: this._config, dmmf });
  }

  parse(): void {
    this._parseEnums();
    this._parseInputTypes();
    this._parseModels();
    this._parseResolvers();
    this._parseOutputTypes();
  }

  private _parseEnums(): void {
    this.enums = this._enumParser.parse();
  }

  private _parseInputTypes(): void {
    ({
      inputTypes: this.inputTypes,
      resolverInputTypes: this.resolverInputTypes,
    } = this._inputTypeParser.parse(this.enums));
  }

  private _parseModels(): void {
    this.models = this._modelParser.parse(this.enums);
  }

  private _parseOutputTypes(): void {
    this.outputTypes = this._outputTypeParser.parse(this.enums);
  }

  private _parseResolvers(): void {
    this.resolvers = this._resolverParser.parse();
  }
}
