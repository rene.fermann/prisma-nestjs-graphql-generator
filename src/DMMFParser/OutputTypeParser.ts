import { compareArrayOfObjects } from "../helpers";
import type { Enum, Field, OutputType } from "../types";
import { OperationType, TypeEnum } from "../types";
import { BaseParser } from "./BaseParser";


export class OutputTypeParser extends BaseParser {
  // eslint-disable-next-line max-lines-per-function
  parse(enums: Enum[]): OutputType[] {
    return (
      this.dmmf.schema.outputObjectTypes.prisma
        .filter(({ name }) => {
          if (name === OperationType.Mutation || name === OperationType.Query) {
            return false;
          }

          if (this.dmmf.datamodel.models.find((model) => model.name === name)) {
            return false;
          }

          return true;
        })
        .sort((a, b) => compareArrayOfObjects({ a, b, field: "name" }))
        // eslint-disable-next-line max-lines-per-function
        .map(({ fields: outputTypeFields, name }) => {
          const fields: Field[] = [];
          const enumImports: Set<string> = new Set();
          const nestJSImports: Set<string> = new Set();
          const outputTypeImports: Set<string> = new Set();

          outputTypeFields.forEach(
            ({ outputType: { isList, location }, outputType, name: fieldName, ...field }) => {
              const type = outputType.type as string;

              const graphQLType = this.parseGraphQLType([
                {
                  isList,
                  location,
                  type,
                },
              ]);

              if (this.nestJSImports.has(type)) {
                nestJSImports.add(type);
              }

              if (this.nestJSImports.has(graphQLType)) {
                nestJSImports.add(graphQLType);
              }

              if (
                location === "outputObjectTypes" &&
                type !== fieldName &&
                this.outputTypeList.has(type)
              ) {
                outputTypeImports.add(type);
              }

              if (location === "enumTypes") {
                enumImports.add(this.getEnumName(type));
              }

              const parsedField = this.parseField({
                enums,
                field: {
                  ...field,
                  ...outputType,
                  isInputType: false,
                  isRequired: false,
                  name: fieldName,
                  type,
                },
                graphQLType,
              });

              fields.push(parsedField);
            }
          );

          return {
            enumImports: Array.from(enumImports),
            fields,
            name,
            nestJSImports: Array.from(nestJSImports),
            outputTypeImports: Array.from(outputTypeImports),
            type: TypeEnum.OutputType,
          };
        })
    );
  }
}
