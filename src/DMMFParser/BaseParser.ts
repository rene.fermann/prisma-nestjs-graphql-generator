import type { DMMF } from "@prisma/generator-helper";

import { pascalCase } from "../helpers";
import type { Enum, Field, GeneratorConfig, ParserConstructorInput } from "../types";
import { NestJSTypes } from "../types";

interface TSField {
  isList: boolean;
  location: DMMF.FieldLocation;
  type: string | DMMF.OutputType | DMMF.SchemaEnum;
}

interface ParseField {
  documentation?: string;
  isInputType: boolean;
  isList: boolean;
  isNullable?: boolean;
  isRequired: boolean;
  location: DMMF.FieldLocation;
  name: string;
  relationName?: string;
  type: string;
}

interface TSFieldOptions {
  isInputType: boolean;
  isRequired: boolean;
}

export class BaseParser {
  protected readonly config: GeneratorConfig;

  protected readonly dmmf: DMMF.Document;

  protected readonly inputTypeList: Set<string> = new Set();

  protected readonly jsonImports = new Set(["InputJsonValue", "JsonValue"]);

  protected readonly nestJSImports = new Set([
    NestJSTypes.Float as string,
    NestJSTypes.GraphQLISODateTime as string,
    NestJSTypes.GraphQLTimestamp as string,
    NestJSTypes.ID as string,
    NestJSTypes.Int as string,
  ]);

  protected readonly outputTypeList: Set<string> = new Set();

  constructor({ config, dmmf }: ParserConstructorInput) {
    this.config = config;
    this.dmmf = dmmf;

    this.dmmf.schema.inputObjectTypes.prisma.forEach((inputType) =>
      this.inputTypeList.add(inputType.name)
    );
    this.dmmf.schema.outputObjectTypes.prisma.forEach((outType) =>
      this.outputTypeList.add(outType.name)
    );
  }

  // eslint-disable-next-line class-methods-use-this
  protected getEnumName(name: string): string {
    return `${name.replace("Enum", "")}Enum`;
  }

  protected getInputTypeName(inputTypeName: string): string {
    return `${pascalCase(inputTypeName)}${pascalCase(this.config.inputArgumentsName)}`;
  }

  protected getModel(name: string): string {
    const model = this.dmmf.mappings.modelOperations.find((modelOperation) =>
      Object.values(modelOperation).includes(name)
    )?.model;

    if (typeof model === "undefined") {
      throw new Error(`Operation ${name} cannot be mapped to a Model`);
    }

    return model;
  }

  // eslint-disable-next-line class-methods-use-this
  protected getResolverName(resolverName: string): string {
    return `${pascalCase(resolverName)}Resolver`;
  }

  // eslint-disable-next-line class-methods-use-this
  protected mapScalarToGraphQLType(scalar: string): string {
    switch (scalar) {
      case "DateTime":
        return "GraphQLISODateTime";

      case "Json":
        return "GraphQLJSON";

      default:
        return scalar;
    }
  }

  // eslint-disable-next-line class-methods-use-this
  protected mapScalarToTSType({
    isInputType = false,
    scalar,
  }: {
    isInputType?: boolean;
    scalar: string;
  }): string {
    switch (scalar) {
      case "Boolean":
        return "boolean";

      case "DateTime":
        return "Date";

      case "Float":
        return "number";

      case "ID":
        return "string";

      case "Int":
        return "number";

      case "Json":
        if (isInputType) {
          return "InputJsonValue";
        }

        return "JsonValue";

      case "String":
        return "string";

      case "UUID":
        return "string";

      default:
        throw new Error(`Unknown scalar type ${scalar}`);
    }
  }

  protected parseField({
    field,
    field: { documentation, isInputType, isRequired, isList, location, name, type, relationName },
    graphQLType,
    enums,
  }: {
    enums: Enum[];
    field: ParseField;
    graphQLType: string;
  }): Field {
    let { isNullable } = field;

    if (typeof isNullable === "undefined" || !isNullable) {
      isNullable = Boolean(relationName) || !isRequired;
    }

    return {
      documentation,
      graphQLType,
      isNullable,
      isRequired,
      name,
      tsType: this.parseTSFieldType({
        enums,
        field: { isList, location, type },
        fieldOptions: {
          isInputType,
          isRequired,
        },
      }),
    };
  }

  protected parseGraphQLType(inputTypes: DMMF.SchemaArgInputType[]): string {
    let inputType = "";
    const inputTypeObject = this.selectInputType(inputTypes);
    const { isList, location, type } = inputTypeObject;

    if (location === "enumTypes") {
      inputType = this.getEnumName(type as string);
    } else {
      inputType = `${type as string}`;
    }

    inputType = this.mapScalarToGraphQLType(inputType);

    if (isList) {
      return `[${inputType}]`;
    }

    return inputType;
  }

  protected parseTSFieldType({
    enums,
    field: { isList, location, type },
    fieldOptions: { isInputType, isRequired },
  }: {
    enums: Enum[];
    field: TSField;
    fieldOptions: TSFieldOptions;
  }): string {
    let fieldType = "";

    if (typeof type !== "string") {
      throw new Error(`Unsupported field type : ${JSON.stringify(type)}`);
    }

    if (location === "enumTypes") {
      if (enums.length < 1) {
        throw new Error(
          "Cannot read enums. Please make sure to pass a valid enum array to this function"
        );
      }

      fieldType = enums
        .find((e) => this.getEnumName(e.name) === this.getEnumName(type))
        ?.values.map((value) => `"${value.name}"`)
        .join(" | ") as string;
    } else if (location === "inputObjectTypes" || location === "outputObjectTypes") {
      fieldType = type;
      // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition
    } else if (location === "scalar") {
      fieldType = this.mapScalarToTSType({ isInputType, scalar: type });
    } else {
      // eslint-disable-next-line @typescript-eslint/restrict-template-expressions
      throw new Error(`Unsupported field location: ${location}`);
    }

    if (isList) {
      if (fieldType.includes(" ")) {
        fieldType = `Array<${fieldType}>`;
      } else {
        fieldType = `${fieldType}[]`;
      }
    }

    if (!isRequired) {
      if (isInputType) {
        fieldType = `${fieldType} | undefined`;
      } else {
        fieldType = `${fieldType} | null`;
      }
    }

    return fieldType;
  }

  // eslint-disable-next-line class-methods-use-this
  protected selectInputType(inputTypes: DMMF.SchemaArgInputType[]): TSField {
    if (inputTypes.length === 0) {
      throw new Error("No input types available. Extracting GraphQL Type not possible");
    }

    const isList = inputTypes.find((inputType) => inputType.isList)?.isList ?? false;

    let inputTypeObject = inputTypes.find(
      (it) => it.location === "inputObjectTypes" || it.location === "outputObjectTypes"
    );

    if (typeof inputTypeObject === "undefined") {
      inputTypeObject = inputTypes.find((it) => it.location === "enumTypes");
    }

    if (typeof inputTypeObject === "undefined") {
      inputTypeObject = inputTypes.find((it) => it.location === "scalar");
    }

    if (typeof inputTypeObject === "undefined") {
      throw new Error("Couldn't parse input type");
    }

    return {
      isList,
      location: inputTypeObject.location,
      type: inputTypeObject.type as string,
    };
  }
}
