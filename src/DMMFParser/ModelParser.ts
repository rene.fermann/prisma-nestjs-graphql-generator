import type { DMMF } from "@prisma/generator-helper";

import type { Enum, Field, Model } from "../types";
import { TypeEnum } from "../types";

import { BaseParser } from "./BaseParser";

const mapKindToLocation = (kind: DMMF.FieldKind): DMMF.FieldLocation => {
  switch (kind) {
    case "enum":
      return "enumTypes";
      break;

    case "object":
      return "inputObjectTypes";
      break;

    case "scalar":
      return "scalar";

    default:
      throw new Error(`Can't map kind ${kind as string} to a location`);
      break;
  }
};

export class ModelParser extends BaseParser {
  parse(enums: Enum[]): Model[] {
    return this.dmmf.datamodel.models.map(({ documentation, fields: modelFields, name }) => {
      const fields: Field[] = [];
      const enumImports: Set<string> = new Set();
      const jsonImports: Set<string> = new Set();
      const modelImports: Set<string> = new Set();
      const nestJSImports: Set<string> = new Set();

      modelFields.forEach((field) => {
        const location = mapKindToLocation(field.kind);

        const graphQLType = this.parseGraphQLType([
          { isList: field.isList, location, type: field.type },
        ]);

        const parsedField = this.parseField({
          enums,
          field: { ...field, isInputType: false, location },
          graphQLType,
        });

        if (field.kind === "enum") {
          enumImports.add(this.getEnumName(field.type));
        }

        if (field.kind === "scalar" && this.jsonImports.has(field.type)) {
          jsonImports.add(field.type);
        }

        if (this.jsonImports.has(parsedField.tsType.split(" | ")[0])) {
          jsonImports.add(parsedField.tsType.split(" | ")[0]);
        }

        if (field.kind === "object") {
          modelImports.add(field.type);
        }

        if (this.nestJSImports.has(field.type)) {
          nestJSImports.add(field.type);
        }

        if (this.nestJSImports.has(graphQLType)) {
          nestJSImports.add(graphQLType);
        }

        fields.push(parsedField);
      });

      return {
        documentation,
        enumImports: Array.from(enumImports),
        fields,
        jsonImports: Array.from(jsonImports),
        modelImports: Array.from(modelImports),
        name,
        nestJSImports: Array.from(nestJSImports),
        type: TypeEnum.ModelType,
      };
    });
  }
}
