import type { DMMF } from "@prisma/generator-helper";

interface Enum {
  documentation?: string;
  name: string;
  values: EnumValue[];
}

interface EnumValue {
  name: string;
  value: string;
}

interface Field {
  documentation?: string;
  graphQLType: string;
  isNullable: boolean;
  isRequired: boolean;
  name: string;
  tsType: string;
}

interface GeneratorConfig {
  basePath: string;
  dmmf: DMMF.Document;
  exportDMMF: boolean;
  inputArgumentsName: string;
  paths: {
    enum: string;
    inputType: string;
    model: string;
    output: string;
    resolver: string;
  };
  prismaClientImportPath: string;
  prismaServiceImport: string;
  prismaServiceImportPath: string;
  includePrismaSelect: boolean;
}

interface InputType {
  enumImports?: string[];
  fields: Field[];
  inputTypeImports?: string[];
  jsonImports?: string[];
  modelImports?: string[];
  name: string;
  nestJSImports?: string[];
  type: TypeEnum.InputType;
}
interface ResolverInputType extends InputType {
  model: string;
  type: TypeEnum.ResolverInputType;
}

interface Model extends InputType {
  documentation: string | undefined;
  type: TypeEnum.ModelType;
}

interface OutputType extends InputType {
  outputTypeImports?: string[];
  type: TypeEnum.OutputType;
}

interface ParserConstructorInput {
  config: GeneratorConfig;
  dmmf: DMMF.Document;
}

interface Resolver {
  fieldType: string;
  graphQLType: string;
  inputType: string;
  isNullable: boolean;
  method: string;
  model: string;
  name: string;
  operation: string;
  operationType: OperationTypeEnum;
}

const enum NestJSTypes {
  "Args" = "Args",
  "Field" = "Field",
  "Float" = "Float",
  "GraphQLISODateTime" = "GraphQLISODateTime",
  "GraphQLTimestamp" = "GraphQLTimestamp",
  "ID" = "ID",
  "Info" = "Info",
  "InputType" = "InputType",
  "Int" = "Int",
  "ObjectType" = "ObjectType",
  "RegisterEnumType" = "registerEnumType",
  "Resolver" = "Resolver",
}

// eslint-disable-next-line @typescript-eslint/no-type-alias
const enum OperationTypeEnum {
  "Mutation" = "Mutation",
  "Query" = "Query",
}

const enum TypeEnum {
  "InputType" = "InputType",
  "ModelType" = "ModelType",
  "OutputType" = "OutputType",
  "ResolverInputType" = "ResolverInputType",
}

export {
  Field,
  Enum,
  EnumValue,
  GeneratorConfig,
  InputType,
  TypeEnum,
  Model,
  NestJSTypes,
  OperationTypeEnum as OperationType,
  OutputType,
  ParserConstructorInput,
  Resolver,
  ResolverInputType,
};
