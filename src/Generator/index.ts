// eslint-disable-next-line import/no-nodejs-modules, import/no-unresolved
import { mkdir, rmdir } from "fs/promises";

import type { GeneratorOptions } from "@prisma/generator-helper";
import { IndentationText, NewLineKind, Project } from "ts-morph";

import { DMMFParser } from "../DMMFParser";
import { FileGenerator } from "../FileGenerator";
import type { GeneratorConfig } from "../types";

class Generator {
  config: GeneratorConfig;

  fileGenerator!: FileGenerator;

  parser: DMMFParser;

  project: Project;

  constructor(options: GeneratorOptions) {
    const { output, config } = options.generator;

    if (output === null) {
      throw new Error("Please define an output directory");
    }

    const prismaClientPath = options.otherGenerators.find(
      (generator) => generator.provider === "prisma-client-js"
    );

    if (typeof prismaClientPath === "undefined" || prismaClientPath.output === null) {
      throw new Error(
        "Cannot detect prisma client. Please make sure to generate `prisma-client-js` first"
      );
    }

    if (config.prismaServiceImportPath.length < 1) {
      throw new Error(
        "Cannot detect prisma service. Please make sure to provide a path to your prisma service"
      );
    }

    this.config = {
      basePath: output,
      dmmf: options.dmmf,
      exportDMMF: config.exportDMMF === "true" || false,
      includePrismaSelect: config.includePrismaSelect === "true" || false,
      inputArgumentsName:
        typeof config.inputArgumentsName === "string" && config.inputArgumentsName.length > 0
          ? config.inputArgumentsName
          : "input",
      paths: {
        enum: "enums",
        inputType: "inputs",
        model: "models",
        output: "output",
        resolver: "resolvers",
      },
      prismaClientImportPath: prismaClientPath.output,
      prismaServiceImport: config.prismaServiceImport || "PrismaService",
      prismaServiceImportPath: config.prismaServiceImportPath,
    };

    this.parser = new DMMFParser(this.config);

    this.project = new Project({
      compilerOptions: {
        emitDecoratorMetadata: true,
        experimentalDecorators: true,
      },
      manipulationSettings: {
        indentationText: IndentationText.TwoSpaces,
        newLineKind: NewLineKind.LineFeed,
      },
    });
  }

  async run(): Promise<void> {
    await this._initTargetFolder();
    await this._parseDMMF();
    this._generateFiles();
    await this.project.save();
  }

  private _generateFiles(): void {
    this.fileGenerator.generateFiles();
  }

  private async _initTargetFolder(): Promise<void> {
    await rmdir(this.config.basePath, { recursive: true });
    await mkdir(this.config.basePath, { recursive: true });
  }

  private async _parseDMMF(): Promise<void> {
    await this.parser.init(this.config.prismaClientImportPath);
    this.parser.parse();
    this.fileGenerator = new FileGenerator({
      config: this.config,
      parser: this.parser,
      project: this.project,
    });
  }
}

export { Generator };
