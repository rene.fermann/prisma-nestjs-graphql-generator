import type { Enum } from "../types";
import { NestJSTypes } from "../types";

import { BaseGenerator } from "./BaseGenerator";

export class EnumGenerator extends BaseGenerator {
  generate(): void {
    const sourceFile = this.createSourceFile(`${this.config.paths.enum}/index.ts`);

    this.parser.enums.forEach((e) => {
      this._generateEnum(e);
      sourceFile.addExportDeclaration({
        moduleSpecifier: `./${e.name}`,
        namedExports: [e.name],
      });
    });
  }

  private _generateEnum(e: Enum): void {
    const sourceFile = this.createSourceFile(`${this.config.paths.enum}/${e.name}.ts`);

    this.addNestJSImports({ nestJSImports: [NestJSTypes.RegisterEnumType], sourceFile });

    sourceFile.addEnum({
      isExported: true,
      members: e.values.map(({ name, value }) => {
        return {
          name,
          value,
        };
      }),
      name: e.name,
    });

    sourceFile.addStatements((writer) =>
      writer
        .writeLine("")
        .writeLine(`${NestJSTypes.RegisterEnumType}(${e.name}, {`)
        .writeLine(`name: "${e.name}",`)
        .conditionalWrite(
          typeof e.documentation === "string",
          `description: "${e.documentation as string}",`
        )
        .writeLine("});")
    );

    sourceFile.formatText();
  }
}
