import type { Project } from "ts-morph";

import type { DMMFParser } from "../DMMFParser";
import type { GeneratorConfig } from "../types";
import { TypeEnum } from "../types";

import { EnumGenerator } from "./EnumGenerator";
import { InputTypeGenerator } from "./InputTypeGenerator";
import { ModelGenerator } from "./ModelGenerator";
import { OutputTypeGenerator } from "./OutputTypeGenerator";
import { ResolverGenerator } from "./ResolverGenerator";

export class FileGenerator {
  private readonly _config: GeneratorConfig;

  private readonly _enumGenerator: EnumGenerator;

  private readonly _inputTypeGenerator: InputTypeGenerator;

  private readonly _modelGenerator: ModelGenerator;

  private readonly _outputTypeGenerator: OutputTypeGenerator;

  private readonly _parser: DMMFParser;

  private readonly _project: Project;

  private readonly _resolverGenerator: ResolverGenerator;

  constructor({
    config,
    parser,
    project,
  }: {
    config: GeneratorConfig;
    parser: DMMFParser;
    project: Project;
  }) {
    this._config = config;
    this._parser = parser;
    this._project = project;
    this._enumGenerator = new EnumGenerator({ config, parser: this._parser, project });
    this._inputTypeGenerator = new InputTypeGenerator({ config, parser: this._parser, project });
    this._modelGenerator = new ModelGenerator({ config, parser: this._parser, project });
    this._outputTypeGenerator = new OutputTypeGenerator({ config, parser: this._parser, project });
    this._resolverGenerator = new ResolverGenerator({ config, parser: this._parser, project });
  }

  generateFiles(): void {
    this._project
      .createSourceFile(`${this._config.basePath}/index.ts`, undefined, {
        overwrite: true,
      })
      .addExportDeclarations([
        {
          moduleSpecifier: `./${this._config.paths.enum}`,
        },
        {
          moduleSpecifier: `./${this._config.paths.inputType}`,
        },
        {
          moduleSpecifier: `./${this._config.paths.model}`,
        },
        {
          moduleSpecifier: `./${this._config.paths.output}`,
        },
        {
          moduleSpecifier: `./${this._config.paths.resolver}`,
        },
      ]);

    if (this._parser.enums.length > 0) {
      this._enumGenerator.generate();
    }

    if (this._parser.resolverInputTypes.length > 0) {
      this._inputTypeGenerator.generate(TypeEnum.ResolverInputType);
    }

    if (this._parser.inputTypes.length > 0) {
      this._inputTypeGenerator.generate(TypeEnum.InputType);
    }

    if (this._parser.models.length > 0) {
      this._modelGenerator.generate();
    }

    if (this._parser.outputTypes.length > 0) {
      this._outputTypeGenerator.generate();
    }

    if (this._parser.resolvers.length > 0) {
      this._resolverGenerator.generate();
    }
  }
}
