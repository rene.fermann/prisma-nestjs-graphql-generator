import type { Model } from "../types";
import { NestJSTypes, TypeEnum } from "../types";

import { BaseGenerator } from "./BaseGenerator";

export class ModelGenerator extends BaseGenerator {
  generate(): void {
    const sourceFile = this.createSourceFile(`${this.config.paths.model}/index.ts`);

    this.parser.models.forEach((model) => {
      const { name } = model;

      this._generateModel(model);
      sourceFile.addExportDeclaration({
        moduleSpecifier: `./${name}`,
        namedExports: [name],
      });
    });
  }

  // eslint-disable-next-line max-lines-per-function
  private _generateModel({
    documentation,
    fields,
    name,
    enumImports,
    jsonImports,
    modelImports,
    nestJSImports,
  }: Model): void {
    const sourceFile = this.createSourceFile(`${this.config.paths.model}/${name}.ts`);

    this.addNestJSImports({
      nestJSImports: [NestJSTypes.Field, NestJSTypes.ObjectType],
      sourceFile,
    });

    if (nestJSImports && nestJSImports.length > 0) {
      this.addNestJSImports({ nestJSImports, sourceFile });
    }

    if (jsonImports && jsonImports.length > 0) {
      this.addJsonImports({ imports: jsonImports, sourceFile });
    }

    if (enumImports && enumImports.length > 0) {
      this.addEnumImports({ enums: enumImports, sourceFile, type: TypeEnum.ModelType });
    }

    if (modelImports && modelImports.length > 0) {
      this.addModelImports({
        models: modelImports,
        sourceFile,
        type: TypeEnum.ModelType,
      });
    }

    sourceFile.addClass({
      decorators: [
        {
          arguments: [
            (writer) =>
              writer
                .write("{")
                .writeLine("isAbstract: true,")
                .conditionalWriteLine(
                  typeof documentation === "string",
                  `description: "${documentation as string}"`
                )
                .write("}"),
          ],
          name: NestJSTypes.ObjectType,
        },
      ],
      docs: typeof documentation === "string" ? [documentation] : [],
      isExported: true,
      name,
      properties: fields.map(
        ({
          documentation: fieldDocumentation,
          graphQLType,
          isNullable,
          isRequired,
          name: fieldName,
          tsType,
        }) => {
          return {
            decorators: [
              {
                arguments: [
                  (writer) =>
                    writer
                      .write(`() => ${graphQLType}, {`)
                      .writeLine(`nullable: ${isNullable},`)
                      .conditionalWriteLine(
                        typeof fieldDocumentation === "string",
                        `description: "${fieldDocumentation as string}",`
                      )
                      .write("}"),
                ],
                name: NestJSTypes.Field,
              },
            ],
            docs: typeof fieldDocumentation === "string" ? [fieldDocumentation] : [],
            hasExclamationToken: isRequired,
            hasQuestionToken: isNullable,
            name: fieldName,
            trailingTrivia: "\r\n",
            type: tsType,
          };
        }
      ),
    });

    sourceFile.formatText();
  }
}
