import type { SourceFile } from "ts-morph";

import { compareArrayOfObjects } from "../helpers";
import type { InputType, ResolverInputType } from "../types";
import { TypeEnum } from "../types";

import { BaseGenerator } from "./BaseGenerator";

export class InputTypeGenerator extends BaseGenerator {
  generate(inputType: TypeEnum): void {
    if (inputType === TypeEnum.ResolverInputType) {
      this.parser.resolverInputTypes.forEach((it) => {
        this._generateInputType(it);
        this._generateResolverInputTypeBarrelFiles();
      });
    } else {
      this.parser.inputTypes.forEach((it) => {
        this._generateInputType(it);
      });
      this._generateInputTypeBarrelFiles();
    }
  }

  private _createInputTypeSourceFile(inputTypeName: string): SourceFile {
    return super.createSourceFile(`${this.config.paths.inputType}/${inputTypeName}.ts`);
  }

  private _createResolverInputTypeSourceFile({
    inputTypeName,
    model,
  }: {
    inputTypeName: string;
    model: string;
  }): SourceFile {
    return super.createSourceFile(
      `${this.config.paths.resolver}/${model}/${this.config.inputArgumentsName}/${inputTypeName}.ts`
    );
  }

  // eslint-disable-next-line max-lines-per-function
  private _generateInputType(inputType: ResolverInputType | InputType): void {
    // eslint-disable-next-line @typescript-eslint/init-declarations
    let sourceFile: SourceFile;

    const {
      enumImports,
      fields,
      inputTypeImports,
      jsonImports,
      name: inputTypeName,
      nestJSImports,
      type,
    } = inputType;

    if (inputType.type === TypeEnum.ResolverInputType) {
      sourceFile = this._createResolverInputTypeSourceFile({
        inputTypeName,
        model: inputType.model,
      });
    } else {
      sourceFile = this._createInputTypeSourceFile(inputTypeName);
    }

    this.addNestJSImports({ nestJSImports, sourceFile });

    this.addEnumImports({
      enums: enumImports,
      sourceFile,
      type,
    });

    this.addInputTypeImports({
      inputTypes: inputTypeImports,
      isResolverInputType: type === TypeEnum.ResolverInputType,
      sourceFile,
    });

    this.addJsonImports({ imports: jsonImports, sourceFile });

    sourceFile.addClass({
      decorators: [
        {
          arguments: [(writer) => writer.writeLine("{").writeLine(`isAbstract: true,`).write("}")],
          name: "InputType",
        },
      ],
      isExported: true,
      name: inputTypeName,
      properties: fields.map(
        ({ graphQLType: graphqlType, isNullable, isRequired, name: propertyName, tsType }) => {
          return {
            decorators: [
              {
                arguments: [
                  (writer) =>
                    writer
                      .writeLine(`() => ${graphqlType}, {`)
                      .writeLine(`nullable: ${isNullable}`)
                      .write("}"),
                ],
                name: "Field",
              },
            ],
            hasExclamationToken: isRequired,
            hasQuestionToken: isNullable,
            name: propertyName,
            trailingTrivia: "\r\n",
            type: tsType,
          };
        }
      ),
    });

    sourceFile.formatText();
  }

  private _generateInputTypeBarrelFiles(): void {
    const sourceFile = super.createSourceFile(`${this.config.paths.inputType}/index.ts`);

    this.parser.inputTypes
      .sort((a, b) => compareArrayOfObjects({ a, b, field: "name" }))
      .forEach(({ name }) => {
        sourceFile.addExportDeclaration({
          moduleSpecifier: `./${name}`,
          namedExports: [name],
        });
      });
  }

  private _generateResolverInputTypeBarrelFiles(): void {
    this.parser.models.forEach(({ name }) => {
      const sourceFile = super.createSourceFile(
        `${this.config.paths.resolver}/${name}/${this.config.inputArgumentsName}/index.ts`
      );

      const inputTypes = this.parser.resolverInputTypes
        .filter((inputType) => inputType.model === name)
        .sort((a, b) => compareArrayOfObjects({ a, b, field: "name" }));

      inputTypes.forEach(({ name: inputTypename }) => {
        sourceFile.addExportDeclaration({
          moduleSpecifier: `./${inputTypename}`,
          namedExports: [inputTypename],
        });
      });
    });
  }
}
