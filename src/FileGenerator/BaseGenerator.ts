import type { Project, SourceFile } from "ts-morph";

import type { DMMFParser } from "../DMMFParser";
import { comparePrimitiveValues } from "../helpers";
import type { GeneratorConfig } from "../types";
import { TypeEnum } from "../types";

export class BaseGenerator {
  protected readonly config: GeneratorConfig;

  protected readonly parser: DMMFParser;

  private readonly _project: Project;

  constructor({
    config,
    parser,
    project,
  }: {
    config: GeneratorConfig;
    parser: DMMFParser;
    project: Project;
  }) {
    this.config = config;
    this.parser = parser;
    this._project = project;
  }

  protected addEnumImports({
    enums,
    sourceFile,
    type,
  }: {
    enums: string[] | undefined;
    sourceFile: SourceFile;
    type: TypeEnum;
  }): void {
    if (typeof enums === "undefined") {
      return;
    }

    if (enums.length < 1) {
      return;
    }

    let moduleSpecifier = "";

    if (
      type === TypeEnum.InputType ||
      type === TypeEnum.ModelType ||
      type === TypeEnum.OutputType
    ) {
      moduleSpecifier = `../${this.config.paths.enum}`;
    }

    if (type === TypeEnum.ResolverInputType) {
      moduleSpecifier = `../../../${this.config.paths.enum}`;
    }

    sourceFile.addImportDeclaration({
      moduleSpecifier,
      namedImports: Array.from(new Set(enums)).sort(comparePrimitiveValues),
    });
  }

  // eslint-disable-next-line class-methods-use-this
  protected addInputTypeImports({
    inputTypes,
    isResolverInputType,
    sourceFile,
  }: {
    inputTypes: string[] | undefined;
    isResolverInputType: boolean;
    sourceFile: SourceFile;
  }): void {
    if (typeof inputTypes === "undefined") {
      return;
    }

    if (inputTypes.length < 1) {
      return;
    }

    let moduleSpecifier = "";

    if (isResolverInputType) {
      moduleSpecifier = "../../../inputs";
    } else {
      moduleSpecifier = "../inputs";
    }

    sourceFile.addImportDeclaration({
      moduleSpecifier,
      namedImports: inputTypes,
    });
  }

  // eslint-disable-next-line class-methods-use-this
  protected addJsonImports({
    imports,
    sourceFile,
  }: {
    imports: string[] | undefined;
    sourceFile: SourceFile;
  }): void {
    if (typeof imports === "undefined") {
      return;
    }

    if (imports.length < 1) {
      return;
    }

    sourceFile.addImportDeclaration({
      moduleSpecifier: "graphql-type-json",
      namedImports: ["GraphQLJSON"],
    });

    sourceFile.addImportDeclaration({
      moduleSpecifier: "@prisma/client",
      namedImports: imports.sort(comparePrimitiveValues),
    });
  }

  protected addModelImports({
    models,
    sourceFile,
    type,
  }: {
    models: string[];
    sourceFile: SourceFile;
    type: TypeEnum;
  }): void {
    let moduleSpecifier = "";

    if (type === TypeEnum.InputType || type === TypeEnum.ModelType) {
      moduleSpecifier = `../${this.config.paths.model}`;
    }

    if (type === TypeEnum.ResolverInputType) {
      moduleSpecifier = `../../${this.config.paths.model}`;
    }

    sourceFile.addImportDeclaration({
      moduleSpecifier,
      namedImports: Array.from(new Set(models)).sort(comparePrimitiveValues),
    });
  }

  // eslint-disable-next-line class-methods-use-this
  protected addNestJSImports({
    nestJSImports,
    sourceFile,
  }: {
    nestJSImports: string[] | undefined;
    sourceFile: SourceFile;
  }): void {
    if (typeof nestJSImports === "undefined") {
      return;
    }

    if (nestJSImports.length < 1) {
      return;
    }

    const importDeclarations = sourceFile.getImportDeclarations();

    const importDeclaration = importDeclarations.find(
      (declaration) => declaration.getModuleSpecifier().getLiteralText() === "@nestjs/graphql"
    );

    if (typeof importDeclaration === "undefined") {
      sourceFile.addImportDeclaration({
        moduleSpecifier: "@nestjs/graphql",
        namedImports: nestJSImports.sort(comparePrimitiveValues),
      });
    } else {
      const namedImports = importDeclaration
        .getNamedImports()
        .map((namedImport) => namedImport.getText());

      namedImports.push(...nestJSImports);
      importDeclaration.removeNamedImports();
      importDeclaration.addNamedImports(
        Array.from(new Set(namedImports)).sort(comparePrimitiveValues)
      );
    }
  }

  protected addOutputTypeImports({
    outputType: outputTypeImports,
    sourceFile,
    type,
  }: {
    outputType: string[] | undefined;
    sourceFile: SourceFile;
    type: TypeEnum;
  }): void {
    if (typeof outputTypeImports === "undefined") {
      return;
    }

    if (outputTypeImports.length < 1) {
      return;
    }

    let moduleSpecifier = "";

    if (type === TypeEnum.ResolverInputType) {
      moduleSpecifier = `../../${this.config.paths.output}`;
    }

    if (type === TypeEnum.OutputType) {
      moduleSpecifier = `../${this.config.paths.output}`;
    }

    sourceFile.addImportDeclaration({
      moduleSpecifier,
      namedImports: outputTypeImports,
    });
  }

  protected addPrismaServiceImport(sourceFile: SourceFile): void {
    sourceFile.addImportDeclaration({
      moduleSpecifier: this.config.prismaServiceImportPath,
      namedImports: [this.config.prismaServiceImport],
    });
  }

  protected createSourceFile(name: string): SourceFile {
    return this._project.createSourceFile(`${this.config.basePath}/${name}`, undefined, {
      overwrite: true,
    });
  }
}
