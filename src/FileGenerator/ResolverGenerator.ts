import type { ExportDeclarationStructure, ParameterDeclarationStructure } from "ts-morph";
import { Scope, StructureKind } from "ts-morph";

import { camelCase } from "../helpers";
import type { Resolver } from "../types";
import { NestJSTypes, TypeEnum } from "../types";

import { BaseGenerator } from "./BaseGenerator";

export class ResolverGenerator extends BaseGenerator {
  generate(): void {
    const barrelFileExports: Map<string, Set<string>> = new Map();

    this.createSourceFile(`${this.config.paths.resolver}/index.ts`).addExportDeclarations(
      this.parser.models.map(({ name }) => {
        barrelFileExports.set(name, new Set());
        return {
          moduleSpecifier: `./${name}`,
        };
      })
    );

    this.parser.resolvers.forEach((resolver) => {
      const { model, name } = resolver;

      barrelFileExports.get(model)?.add(name);

      this._generateCrudResolver(resolver);
    });

    this.parser.models.forEach(({ name }) => {
      const exports = barrelFileExports.get(name);

      if (exports) {
        const exportDeclarations: ExportDeclarationStructure[] = [];

        exports.forEach((exportName) => {
          exportDeclarations.push({
            kind: StructureKind.ExportDeclaration,
            moduleSpecifier: `./${exportName}`,
            namedExports: [exportName],
          });
        });

        this.createSourceFile(
          `./${this.config.paths.resolver}/${name}/index.ts`
        ).addExportDeclarations(exportDeclarations);
      }
    });
  }

  // eslint-disable-next-line max-lines-per-function
  private _generateCrudResolver({
    fieldType,
    graphQLType,
    inputType,
    isNullable,
    method,
    model,
    name,
    operation,
    operationType,
  }: Resolver): void {
    const sourceFile = this.createSourceFile(`${this.config.paths.resolver}/${model}/${name}.ts`);
    const isAggregation = operation === "aggregate";

    this.addPrismaServiceImport(sourceFile);
    this.addNestJSImports({
      nestJSImports: [NestJSTypes.Args, NestJSTypes.Resolver, operationType],
      sourceFile,
    });

    if (this.config.includePrismaSelect) {
      sourceFile.addImportDeclarations([
        {
          moduleSpecifier: "graphql",
          namedImports: ["GraphQLResolveInfo"],
        },
        {
          moduleSpecifier: "@paljs/plugins",
          namedImports: ["PrismaSelect"],
        },
      ]);
      this.addNestJSImports({
        nestJSImports: [NestJSTypes.Info],
        sourceFile,
      });
    }

    this.addModelImports({ models: [model], sourceFile, type: TypeEnum.ResolverInputType });

    let outputType = "";
    let aggregateType = "";

    if (fieldType.includes("[]")) {
      [outputType] = fieldType.split("[]");
    } else if (fieldType.includes("|")) {
      [outputType] = fieldType.split(" | ");
    } else {
      outputType = fieldType;
    }

    const isBatchOperation = outputType === "BatchPayload";

    if (isAggregation) {
      aggregateType = `Prisma.Get${model}AggregateType<${inputType}>`;
      sourceFile.addImportDeclaration({
        moduleSpecifier: "@prisma/client",
        namedImports: [`Prisma`],
      });
    }

    if (outputType !== model) {
      this.addOutputTypeImports({
        outputType: [outputType],
        sourceFile,
        type: TypeEnum.ResolverInputType,
      });
    }

    sourceFile.addImportDeclaration({
      moduleSpecifier: `./${this.config.inputArgumentsName}`,
      namedImports: [inputType],
    });

    const parameters: ParameterDeclarationStructure[] = [
      {
        decorators: [
          {
            arguments: [`"${this.config.inputArgumentsName}", { nullable: true }`],
            kind: StructureKind.Decorator,
            name: NestJSTypes.Args,
          },
        ],
        kind: StructureKind.Parameter,
        name: this.config.inputArgumentsName,
        type: inputType,
      },
    ];

    if (this.config.includePrismaSelect) {
      parameters.push({
        decorators: [
          {
            arguments: [],
            kind: StructureKind.Decorator,
            name: NestJSTypes.Info,
          },
        ],
        kind: StructureKind.Parameter,
        name: "info",
        type: "GraphQLResolveInfo",
      });
    }

    sourceFile.addClass({
      ctors: [
        {
          parameters: [
            {
              name: "prismaService",
              type: this.config.prismaServiceImport,
            },
          ],
          statements: ["this._prismaService = prismaService"],
        },
      ],
      decorators: [
        {
          arguments: [`() => ${model}`],
          name: NestJSTypes.Resolver,
        },
      ],
      isExported: true,
      methods: [
        {
          decorators: [
            {
              arguments: [
                (writer) =>
                  writer
                    .writeLine(`() => ${graphQLType}, {`)
                    .writeLine(`nullable: ${isNullable}`)
                    .write("}"),
              ],
              name: operationType,
            },
          ],
          isAsync: true,
          name: camelCase(method),
          parameters,
          returnType: (writer) =>
            writer
              .conditionalWrite(
                this.config.includePrismaSelect && !isAggregation,
                `Promise<${
                  aggregateType || fieldType.replace(outputType, `Partial<${outputType}>`)
                }>`
              )
              .conditionalWrite(
                !this.config.includePrismaSelect || isAggregation,
                `Promise<${aggregateType || fieldType}>`
              ),
          statements: (writer) =>
            writer
              .conditionalWriteLine(
                this.config.includePrismaSelect && !isAggregation && !isBatchOperation,
                `const { select } = new PrismaSelect(info).value;`
              )
              .conditionalWriteLine(
                this.config.includePrismaSelect && isAggregation,
                `const { count } = new PrismaSelect(info).value;`
              )
              .conditionalWriteLine(
                this.config.includePrismaSelect && !isAggregation && !isBatchOperation,
                `return this._prismaService.${camelCase(model)}.${operation}({ select, ...${
                  this.config.inputArgumentsName
                } })`
              )
              .conditionalWriteLine(
                this.config.includePrismaSelect && isAggregation,
                `return this._prismaService.${camelCase(model)}.${operation}({ count, ...${
                  this.config.inputArgumentsName
                } })`
              )
              .conditionalWriteLine(
                (!this.config.includePrismaSelect && !isAggregation) || isBatchOperation,
                `return this._prismaService.${camelCase(model)}.${operation}({ ...${
                  this.config.inputArgumentsName
                } })`
              ),
        },
      ],
      name,
      properties: [
        {
          isReadonly: true,
          name: "_prismaService",
          scope: Scope.Private,
          type: this.config.prismaServiceImport,
        },
      ],
    });

    sourceFile.formatText();
  }
}
