import type { OutputType } from "../types";
import { NestJSTypes, TypeEnum } from "../types";

import { BaseGenerator } from "./BaseGenerator";

export class OutputTypeGenerator extends BaseGenerator {
  generate(): void {
    const sourceFile = this.createSourceFile(`${this.config.paths.output}/index.ts`);

    this.parser.outputTypes.forEach((outputType) => {
      const { name } = outputType;

      this._generateOutputType(outputType);
      sourceFile.addExportDeclaration({
        moduleSpecifier: `./${name}`,
        namedExports: [name],
      });
    });
  }

  // eslint-disable-next-line max-lines-per-function
  private _generateOutputType({
    enumImports,
    fields,
    name,
    nestJSImports,
    outputTypeImports,
  }: OutputType): void {
    const sourceFile = this.createSourceFile(`${this.config.paths.output}/${name}.ts`);

    this.addNestJSImports({
      nestJSImports: [NestJSTypes.Field, NestJSTypes.ObjectType],
      sourceFile,
    });

    this.addNestJSImports({
      nestJSImports,
      sourceFile,
    });

    if (enumImports && enumImports.length > 0) {
      this.addEnumImports({ enums: enumImports, sourceFile, type: TypeEnum.OutputType });
    }

    this.addOutputTypeImports({
      outputType: outputTypeImports,
      sourceFile,
      type: TypeEnum.OutputType,
    });

    sourceFile.addClass({
      decorators: [
        {
          arguments: [(writer) => writer.write("{").writeLine("isAbstract: true,").write("}")],
          name: NestJSTypes.ObjectType,
        },
      ],
      isExported: true,
      name,
      properties: fields.map(({ graphQLType, isNullable, isRequired, name: fieldName, tsType }) => {
        return {
          decorators: [
            {
              arguments: [
                (writer) =>
                  writer
                    .write(`() => ${graphQLType}, {`)
                    .writeLine(`nullable: ${isNullable},`)
                    .write("}"),
              ],
              name: NestJSTypes.Field,
            },
          ],
          hasExclamationToken: isRequired,
          hasQuestionToken: isNullable,
          name: fieldName,
          trailingTrivia: "\r\n",
          type: tsType,
        };
      }),
    });

    sourceFile.formatText();
  }
}
